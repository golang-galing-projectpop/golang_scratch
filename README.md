# golang_scratch

corat coret belajar golang

# Notes

catatan pribadi belajar dasar-dasar Golang.

# First Time Using Golang
- Download

        Download Golang => https://golang.org/

- Install
        
        Running Installer *.exe
        
- Check

        Terminal => "go version"
        
- Init Project

        mkdir <project-name>
        cd <project-name>
        go mod init <project-name>
        
# Running Go

- Running Files

        cd <project-name>
        go run main.go
        
# Practice

buat sebuah file dengan nama main.go dan pastikan nama package adalah "main"


        package main
        
        import "fmt"
        
        func main() {
            fmt.Println("Hello world")
        }
        
coba jalankan

        go run main.go
        
output (pada konteks ini, terminal atau CMD) :

        Hello world
        
# Reference

- https://dasarpemrogramangolang.novalagung.com/